import { NgModule } from '@angular/core';
import { HelloPipe } from './hello/hello';
@NgModule({
	declarations: [HelloPipe],
	imports: [],
	exports: [HelloPipe]
})
export class PipesModule {}
