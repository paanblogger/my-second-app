import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the HelloPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'hello',
})
export class HelloPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string, ...args) {
    return "Hello "+value;
  }
}
