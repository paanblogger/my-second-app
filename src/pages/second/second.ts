import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {LocalProvider} from '../../providers/local/local';
/**
* Generated class for the SecondPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/
@IonicPage()
@Component({
	selector: 'page-second',
	templateUrl: 'second.html',
})
export class SecondPage {
	first_name:any;
	saved_number:any;
	constructor(public navCtrl: NavController, public navParams: NavParams , public local:LocalProvider) {
	}
	ionViewDidLoad() {
		console.log('ionViewDidLoad SecondPage');
		this.first_name = this.navParams.get("first_name");
		this.local.readLocal('total')
		.then(resp => {
			this.saved_number = resp;
		},err => {
			console.log("Error" , err);
		})
	}
	deleteTotal()
	{
		this.local.deleteLocal("total");
		this.saved_number = null;
	}
	check()
	{
		return this.saved_number != null;
	}
}