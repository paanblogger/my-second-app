import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , ViewController } from 'ionic-angular';

/**
 * Generated class for the AddModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-modal',
  templateUrl: 'add-modal.html',
})
export class AddModalPage {
	task:any;
	priority:any;
  constructor(public navCtrl: NavController, public navParams: NavParams , public vc:ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddModalPage');
  }
  close()
  {
  	this.vc.dismiss(0);
  }
  sendData()
  {
  	this.vc.dismiss({task:this.task,priority:this.priority});
  }
}
