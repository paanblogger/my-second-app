import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , LoadingController } from 'ionic-angular';
import {HomePage} from '../home/home';
import {FirebaseProvider} from '../../providers/firebase/firebase';
/**
* Generated class for the LoginPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/
@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class LoginPage {
    email:any;
    password:any;
    message:any;
    currentuser:any;
    constructor(public navCtrl: NavController, public navParams: NavParams , public firebase:FirebaseProvider , public loadingCtrl: LoadingController) {
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad LoginPage');
        this.firebase.checkUser()
        .then(resp_user => {
            if(resp_user)
            {
                //console.log(resp_user.email);
                this.navCtrl.setRoot(HomePage);
                //alert("login");
            }
            else
            {
                //alert("Not login");
            }
        },err => {
            //alert("Not login");
        });
        this.currentuser = this.firebase.currentUser();
        console.log("Current User" , this.currentuser);
    }
    reset()
    {
        this.email = null;
        this.password = null;
    }
    login()
    {
        let loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });

        loading.present();
        this.firebase.login(this.email , this.password)
        .then(resp => {
            this.navCtrl.setRoot(HomePage);
            loading.dismiss();
            // alert('Success');
            // console.log("Login" , resp);
            // this.currentuser = this.firebase.currentUser();
            // console.log("Current User" , this.currentuser.email);
        } , err => {
            loading.dismiss();
            this.message = "Failed";
            //alert(this.message);
        });
    }
    logout()
    {
        this.firebase.logout();
        console.log("Logout");
    }
}