import { Component } from '@angular/core';
import { NavController ,ModalController } from 'ionic-angular';
import {SecondPage} from '../second/second';
import {AddModalPage} from '../add-modal/add-modal';
import {CalculateProvider} from '../../providers/calculate/calculate';
import {LocalProvider} from '../../providers/local/local';
import {NetworkProvider} from '../../providers/network/network';
import { Camera, CameraOptions } from '@ionic-native/camera';
import {FirebaseProvider} from '../../providers/firebase/firebase';
@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage {
	first_name:any;
	total:any;
	first_number:any;
	second_number:any;
	the_list:any=[];
	new_list:any=[];
	image_taken:any;
	constructor(public navCtrl: NavController , public cp:CalculateProvider , public local:LocalProvider , public netPro:NetworkProvider , private camera: Camera,private modalCtrl:ModalController , private firebase:FirebaseProvider) {
	}
	ionViewDidLoad()
	{
		this.netPro.getData().then((resp_user:any=[]) => {
			this.the_list = resp_user;
			//console.log("The list" , this.the_list);
		},err => {
			//console.log('Error' , err);
		});
		this.netPro.postData();
		this.firebase.createData("33zIl538T3ZRlcAoFuRRy4U7Ude2" , {test:"testvalue"});
		this.firebase.readData("33zIl538T3ZRlcAoFuRRy4U7Ude2");
	}
	login()
	{
		this.navCtrl.push(SecondPage , {first_name:this.first_name});
	}
	calculate()
	{
		this.total = this.cp.addition(this.first_number,this.second_number);
		this.local.writeToLocal("total" , this.total);
		console.log("Addition" , this.total);
	}
	takePicture()
	{
		const options: CameraOptions = {
			quality: 100,
			targetHeight:100,
			targetWidth:100,
			destinationType: this.camera.DestinationType.DATA_URL,
			encodingType: this.camera.EncodingType.JPEG,
			mediaType: this.camera.MediaType.PICTURE
		}
		this.camera.getPicture(options).then((imageData) => {
			let base64Image = 'data:image/jpeg;base64,' + imageData;
			this.image_taken = base64Image;
		}, (err) => {
		});
	}
	gotoModal()
	{
		//this.navCtrl.push(AddModalPage);
		let xyz = this.modalCtrl.create(AddModalPage);
		xyz.onDidDismiss(data => {
			 //1 . Push data to new list 
			this.new_list.push(data);
			/* 2 . Push data to firebase */
		});
		xyz.present();
	}
}