import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { ComponentsModule } from '../components/components.module';
import { PipesModule } from '../pipes/pipes.module';
import { IonicStorageModule } from '@ionic/storage';
import { HttpModule } from '@angular/http';
import { Camera } from '@ionic-native/camera';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import {SecondPage} from '../pages/second/second';
import {LoginPage} from '../pages/login/login';
import {AddModalPage} from '../pages/add-modal/add-modal';

import * as firebase from 'firebase';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { CalculateProvider } from '../providers/calculate/calculate';
import { LocalProvider } from '../providers/local/local';
import { NetworkProvider } from '../providers/network/network';
import { FirebaseProvider } from '../providers/firebase/firebase';


// Initialize Firebase
  var config = {
    apiKey: "AIzaSyBOhLplx0yF6ikdSMhpntWyZPiad4hXvqU",
    authDomain: "fir-class-e37f9.firebaseapp.com",
    databaseURL: "https://fir-class-e37f9.firebaseio.com",
    projectId: "fir-class-e37f9",
    storageBucket: "fir-class-e37f9.appspot.com",
    messagingSenderId: "187222863630"
  };
  firebase.initializeApp(config);
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    SecondPage,
    LoginPage,
    AddModalPage
  ],
  imports: [
    HttpModule,
    BrowserModule,
    ComponentsModule,
    PipesModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    SecondPage,
    LoginPage,
    AddModalPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CalculateProvider,
    LocalProvider,
    NetworkProvider,
    Camera,
    FirebaseProvider
  ]
})
export class AppModule {}