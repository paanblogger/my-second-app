import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
/*
Generated class for the FirebaseProvider provider.
See https://angular.io/guide/dependency-injection for more info on providers
and Angular DI.
*/
@Injectable()
export class FirebaseProvider {
	constructor() {
		console.log('Hello FirebaseProvider Provider');
	}
	login(email,password)
	{
		return new Promise((resolve,reject) => {
			firebase.auth().signInWithEmailAndPassword(email,password)
			.then(resp_login => {
				resolve(resp_login);
			},err => {
				reject(err);
			})
		});
	}
	currentUser()
	{
		return firebase.auth().currentUser;
	}
	checkUser()
	{
		return new Promise((resolve,reject) => {
			firebase.auth().onAuthStateChanged(user => {
				resolve(user);
			},err => {
				reject(err);
			});
		});
	}
	logout()
	{
		firebase.auth().signOut().then(resp => {
			console.log("Success logout" , resp);
		},err => {
			console.log("Failed logout" , err);
		});
	}
	createData(uid , new_data)
	{
		return new Promise((resolve,reject) => {
			var newPostKey = firebase.database().ref().child("user_todo").push().key;
			firebase.database().ref(uid+"/user_todo/"+newPostKey+'/').set(new_data)
			.then(response => {
				resolve(response);
			},err => {
				reject(err);
			})
		});
	}

	readData(uid)
	{
		return new Promise((resolve,reject) => {
			firebase.database().ref(uid+"/user_todo/").once('value')
			.then(snapshot => {
				let the_data=[];
				snapshot.forEach(abc => {
					console.log("Read data snapshot",abc.val());
					the_data.push({
						value:abc.val(),
						key:abc.key,
					});

				});
				console.log(the_data);
				resolve(the_data);
			},err => {
				console.log("Read data error" , err);
				reject(err);
			})
		});
	}
	updateData(uid,post_id,new_data)
	{

	}
	deleteData(uid,post_id)
	{

	}
}