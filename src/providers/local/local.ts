import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
/*
Generated class for the LocalProvider provider.
See https://angular.io/guide/dependency-injection for more info on providers
and Angular DI.
*/
@Injectable()
export class LocalProvider {
    constructor(private store:Storage) {
        console.log('Hello LocalProvider Provider');
    }
    writeToLocal(key,data)
    {
        this.store.set(key, data)
        .then(_ =>{
            console.log("Success delete");
        });
    }
    readLocal(key)
    {
        return new Promise((resolve,reject) => {
            this.store.get(key)
            .then(data => {
                console.log("All Good");
                resolve(data);
            },err=> {
                console.log("Error");
                reject(err);
            })
        });
    }
    deleteLocal(key)
    {
        this.store.remove(key)
        .then(_=>{
            console.log("Success delete");
        },err=>{
            console.log("Failed delete",err);
        });
    }
}