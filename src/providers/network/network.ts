import { Http,Headers,RequestOptions  } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
/*
Generated class for the NetworkProvider provider.
See https://angular.io/guide/dependency-injection for more info on providers
and Angular DI.
*/
@Injectable()
export class NetworkProvider {
	url = "https://jsonplaceholder.typicode.com/users";
	constructor(public http: Http) {
		console.log('Hello NetworkProvider Provider');
	}
	getData()
	{
		return new Promise((resolve,reject) => {
			this.http.get(this.url)
			.map(res => res.json())
			.subscribe(resp_http => {
				//console.log('Response : ' , resp_http);
				resolve(resp_http);
			},err=> {
				//console.log('Error');
				reject(err);
			});
		});
	}
	postData()
	{
		let headers = new Headers(
		{
			//'Content-Type' : 'application/x-www-form-urlencoded',
			'Content-Type' : 'application/json',
		});
		let options = new RequestOptions({ headers: headers });

		let data = JSON.stringify({
			location_id: 1,
			name: "Test mobile",
			email: "testmobile@app.com",
			phone: "60192950480",
			message: "Test submit suggestion mobile",
		});
		//let data = "location_id="+1+"&name=farhan&email=testmobile@app.com&phone=60192950480&message=Testingmobile";
		return new Promise((resolve,reject) => {
			this.http.post("http://peticadangan.local/api/suggestion" , data ,options)
			.map(res => res.json())
			.subscribe(resp_http => {
				resolve(resp_http);
				console.log("POST" , resp_http);
			},err => {
				reject(err);
				console.log("POST Error" , err);
			})
		});
	}
}