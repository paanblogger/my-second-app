import { Injectable } from '@angular/core';
/*
Generated class for the CalculateProvider provider.
See https://angular.io/guide/dependency-injection for more info on providers
and Angular DI.
*/
@Injectable()
export class CalculateProvider {
	constructor() {
		console.log('Hello CalculateProvider Provider');
	}
	addition(x,y)
	{
		return Number(x)+Number(y);
	}
}